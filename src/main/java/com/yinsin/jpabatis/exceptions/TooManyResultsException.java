package com.yinsin.jpabatis.exceptions;

public class TooManyResultsException extends PersistenceException {
	private static final long serialVersionUID = -2541942608966159517L;

	public TooManyResultsException() {
	}

	public TooManyResultsException(String message) {
		super(message);
	}

	public TooManyResultsException(String message, Throwable cause) {
		super(message, cause);
	}

	public TooManyResultsException(Throwable cause) {
		super(cause);
	}
}
