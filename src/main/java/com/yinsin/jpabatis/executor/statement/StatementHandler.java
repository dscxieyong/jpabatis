/**
 *    Copyright 2009-2016 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.yinsin.jpabatis.executor.statement;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.data.domain.Page;

import com.yinsin.jpabatis.exceptions.JpaBatisException;
import com.yinsin.jpabatis.executor.parameter.ParameterHandler;
import com.yinsin.jpabatis.mapper.BoundSql;
import com.yinsin.jpabatis.session.RowBounds;

/**
 * @author Clinton Begin
 */
public interface StatementHandler {

	BoundSql getBoundSql();

	ParameterHandler getParameterHandler();

	<T> T query(EntityManager transaction) throws JpaBatisException;

	<T> List<T> queryList(EntityManager transaction) throws JpaBatisException;
	
	<T> Page<T> queryList(EntityManager transaction, RowBounds rowBounds) throws JpaBatisException;
}
